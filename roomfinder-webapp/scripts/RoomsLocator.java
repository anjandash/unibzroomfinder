import java.awt.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;


/**
 * 
 * @author LucaPellegrini
 * Easy program --> it just takes the today date and looks for rooms (not yet found) looking the calendar in the next days
 * At the end a txt is generated with the found rooms (distinct)
 */

public class RoomsLocator {
	
	

	public static void main(String[] args) throws IOException {
		LinkedList<String> list = new LinkedList<String>();
		
	
		Date d = new Date();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");	
		SimpleDateFormat sdfM = new SimpleDateFormat("MM");	
		SimpleDateFormat sdfd = new SimpleDateFormat("dd");	
		
		//System.out.println(dateString);
  	
		String url = ("https://webservices.scientificnet.org/rest/cm/core/api/Timetable/GetEvents/?q={%22Day%22:%22"+sdf.format(d)+"-"+sdfM.format(d)+"-%20"+sdfd.format(d)+"T00:00:00Z%22}");
		int count = 300;
	   
		while (count > 0){
	    url = ("https://webservices.scientificnet.org/rest/cm/core/api/Timetable/GetEvents/?q={%22Day%22:%22"+sdf.format(d)+"-"+sdfM.format(d)+"-%20"+sdfd.format(d)+"T00:00:00Z%22}");

			
       JSONArray array = readJsonFromUrl(url);
//       System.out.println(array.length());
       for (int i = 0; i<array.length(); i++){
    	   JSONObject event = array.getJSONObject(i);
    	   String room = event.getString("Room");
    	   if (!list.contains(room)){
    	   		list.add(room);
    	   		System.out.println(room + " added");
    	   }
    	   
       }
       System.out.println(count + " left");
       count--;
       Calendar c = Calendar.getInstance();
       c.setTime(d);
       c.add(Calendar.DATE, 1);  // number of days to add
       d = c.getTime();
		}
		System.out.println(list.size() +" rooms found");
		File fout = new File("rooms.txt");
		FileOutputStream fos = new FileOutputStream(fout);
	 
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
	 
		
		
		for (String line : list){
			bw.write(line);
			bw.newLine();
		}
		
		bw.close();
	}	
	
	private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }

	  public static JSONArray readJsonFromUrl(String url) throws IOException, JSONException {
	    InputStream is = new URL(url).openStream();
	    try {
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	      String jsonText = readAll(rd);
	      JSONArray json = new JSONArray(jsonText);
	      return json;
	    } finally {
	      is.close();
	    }
	  }
	
}