import { NgModule }             from '@angular/core';

import { BrowserModule }        from '@angular/platform-browser';
import { HttpModule }           from '@angular/http';

import { AppRoutingModule }     from './app-routing/app-routing.module';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryRoomService }  from './services/static-rooms-web-api.service';

import { AppComponent }         from './app.component';
import { RoomComponent }        from './room/room.component';

import { RoomService }          from './services/room.service';
import { RoomsComponent }       from './rooms/rooms.component';
import { TimetableService }          from './services/timetable.service';
import { TimetableComponent }   from './timetable/timetable.component';

import { HelpComponent }        from './help/help.component';
import { HomeComponent }        from './home/home.component';
import { FormsModule }          from '@angular/forms';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryRoomService, {
      passThruUnknownUrl: true
    }),
    AppRoutingModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    RoomComponent,
    HelpComponent,
    RoomsComponent,
    TimetableComponent,
    HomeComponent
  ],
  providers: [
    RoomService,
    TimetableService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
