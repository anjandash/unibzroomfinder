export class Lecture {
  public Start: Date;
  public End: Date;
  public Date: Date;
  HideEndTime: boolean;
  StaffAcademicTitle: string;
  StaffLastname: string;
  StaffFirstname: string;
  ProfHrisId: string;
  ActivityDescription: string;
  ActivityType: string;
  BuildingName: string;
  City: string;
  Room: string;
  RoomDescription: string;
  RIS_departmentId: string;
  DepartmentId: string;
  ProfImageUrl: string;

}
