export class Room {
  id: number;
  city: number;
  building: number;
  name: string;
  desc: string;

public static compare(r1: Room, r2: Room): number {
    const nameA = r1.name.toLowerCase(), nameB = r2.name.toLowerCase();
    if (nameA < nameB) {
     return -1;
    } else if (nameA > nameB) {
     return 1;
    }
    return 0;
  }

}
