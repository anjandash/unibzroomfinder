
import { Room } from '../classes/room';

export class FreeRoom {
  Room: Room;
  From: Date;
  To: Date;
  building: string;

  public getFromTimeString(): string {
    return this.From.getHours() + ':' + this.From.getMinutes();
  }

  public getToTimeString(): string {
    return this.To.getHours() + ':' + this.To.getMinutes();
  }

  public getBuilding(): string {
   const name1 = this.Room.name.split(' ')[1];
   const buildingName = name1.split('')[0];
   return buildingName;
  }

  public getFloor(): string {
   const floorName = this.Room.name.split(' ')[1];
   const floorName1 = floorName.split('')[1];
   return floorName1;
  }

}
