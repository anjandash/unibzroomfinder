export class City {

  public static CITY_ID_INVALID = -1;
  public static CITY_ID_BOLZANO = 1;
  public static CITY_ID_BRESSANONE = 2;
  public static CITY_ID_BRUNICO = 3;

  public static CITY_OPTIONS = [
    { 'id' : City.CITY_ID_INVALID, 'name': '---' },
    { 'id' : City.CITY_ID_BOLZANO, 'name': 'Bozen' },
    { 'id' : City.CITY_ID_BRESSANONE, 'name': 'Bressanone' },
    { 'id' : City.CITY_ID_BRUNICO, 'name': 'Brunico' },
  ];

  id: number;
  name_de: string;
  name_it: string;

  public static isValidCity(city: City): boolean {
    return this.isValidCityId(city.id);
  }

  public static isValidCityId(cityId: number): boolean {
    switch (cityId) {
      case City.CITY_ID_BOLZANO:
      case City.CITY_ID_BRESSANONE:
      case City.CITY_ID_BRUNICO:
        return true;
      default:
        return false;
    }
  }

}
