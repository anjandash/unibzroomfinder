import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Room } from './classes/room';
import { RoomService } from './services/room.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'UnibzRoomFinder';

  constructor(private roomService: RoomService,
              private router: Router) { }

  ngOnInit(): void {
  }
}
