import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

import { RoomService } from '../services/room.service';
import { Room } from '../classes/room';

@Component({
  selector: 'rooms-component',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

  rooms: Room[];

  constructor(
      private roomService: RoomService,
      private route: ActivatedRoute,
      private location: Location
  ) { }

  ngOnInit() {
    this.getRooms();
  }

  getRooms(): void {
    this.roomService.getRooms().then(rooms => this.rooms = rooms);
  }

}
