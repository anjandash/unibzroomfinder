import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomsComponent } from './rooms.component';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RoomService } from '../services/room.service';
import { TimetableService } from '../services/timetable.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('RoomsComponent', () => {
  let component: RoomsComponent;
  let fixture: ComponentFixture<RoomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
            imports: [
        HttpModule,
        FormsModule,
        RouterTestingModule,
    ],
      declarations: [ RoomsComponent ],
      providers: [
        RoomService,
        TimetableService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(async() => {
    fixture = TestBed.createComponent(RoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    fixture.componentInstance.getRooms();
  });

  // Not useful to test this, because there is no really testable method in this class.
  // it('expect rooms to be not empty', () => {
  //   expect(fixture.componentInstance.rooms.length).toBe(5);
  // });
});
