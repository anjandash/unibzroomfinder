import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReflectiveInjector } from '@angular/core';
import {fakeAsync, tick} from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, RequestOptions } from '@angular/http';
import {Response, ResponseOptions} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RoomComponent } from './room.component';
import { RoomService } from '../services/room.service';
import { TimetableService } from '../services/timetable.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('RoomComponent', () => {
  let component: RoomComponent;
  let fixture: ComponentFixture<RoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        FormsModule,
        RouterTestingModule,
      ],
      declarations: [ RoomComponent ],
      providers: [
        RoomService,
        TimetableService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});


const mockResponseLectures = [
  {
  Start: '2017-12-06T08:30:00',
  End: '2017-12-06T10:30:00',
  Date: '2017-12-06T00:00:00',
  HideEndTime: false,
  StaffAcademicTitle: 'Dr.',
  StaffLastname: 'Levaggi',
  StaffFirstname: 'Laura',
  ProfHrisId: 27466,
  ActivityDescription: 'Analisi Matematica 1',
  ActivityType: 'LECT',
  BuildingName: 'Ser-C',
  City: 'Bolzano',
  Room: 'BZ C4.01',
  RoomDescription: 'C4.01 Lecture room, Ser-C',
  RIS_departmentId: 9475,
  DepartmentId: 370,
  ProfImageUrl: 'https://webservices.scientificnet.org/rest/cm/core/api/Image/Get/?domain=unibz&username=llevaggi'
  },
  {
  Start: '2017-12-06T08:30:00',
  End: '2017-12-06T10:30:00',
  Date: '2017-12-06T00:00:00',
  HideEndTime: false,
  StaffAcademicTitle: 'Dr. Ing.',
  StaffLastname: 'Concli',
  StaffFirstname: 'Franco',
  ProfHrisId: 34279,
  ActivityDescription: 'Advanced topics on machine design',
  ActivityType: 'EXERCISE',
  BuildingName: 'Ser-C',
  City: 'Bolzano',
  Room: 'BZ C2.02',
  RoomDescription: 'C2.02 Seminar room, Ser-C',
  RIS_departmentId: 9475,
  DepartmentId: 370,
  ProfImageUrl: 'https://webservices.scientificnet.org/rest/cm/core/api/Image/Get/?domain=unibz&username=FConcli'
  }
];
