import { Component, OnInit }        from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';

import { TimetableService }         from '../services/timetable.service';

import { Lecture }                  from '../classes/lecture';

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.css']
})
export class TimetableComponent implements OnInit {

  lectures: Lecture[];

  constructor(
      private timetableService: TimetableService,
      private route: ActivatedRoute,
      private location: Location
  ) { }

  ngOnInit() {
    this.getLectures();
  }

  getLectures() {
    this.timetableService.getLecturesTest().then(lectures => this.lectures = lectures);
  }
}
