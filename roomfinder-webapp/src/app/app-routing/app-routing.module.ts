import { NgModule }               from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';

import { AppComponent }       from '../app.component';
import { RoomsComponent }     from '../rooms/rooms.component';
import { RoomComponent }      from '../room/room.component';
import { TimetableComponent } from '../timetable/timetable.component';
import { HelpComponent }      from '../help/help.component';
import { HomeComponent }      from '../home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'rooms', component: RoomsComponent },
  { path: 'room/:id', component: RoomComponent },
  { path: 'timetable', component: TimetableComponent },
  { path: 'help', component: HelpComponent },
  { path: 'home', component: HomeComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
