import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { City } from '../classes/city';
import { Building } from '../classes/building';
import { Room } from '../classes/room';
import { Lecture } from '../classes/lecture';
import { FreeRoom } from '../classes/free-room';
import { TimetableService } from './timetable.service';

@Injectable()
export class RoomService {

  private apiUrl = 'api';  // URL to web api
  private headers = new Headers({'Content-Type': 'application/json'});

  lectures: Lecture[];
  constructor(private http: Http, private tmtblServ: TimetableService) { }

  getCities(): Promise<City[]> {
    return this.http.get(`${this.apiUrl}/cities`)
             .toPromise()
             .then(response => response.json() as City[])
             .catch(this.handleError);
  }

  getBuildings(): Promise<Building[]> {
    return this.http.get(`${this.apiUrl}/buildings`)
             .toPromise()
             .then(response => response.json() as Building[])
             .catch(this.handleError);
  }

  getBuildingsInCity(city: number): Promise<Building[]> {
    return this.getBuildings().then(buildings => {
      return buildings.filter(building => building.city === city);
    });
  }

  getRooms(): Promise<Room[]> {
    return this.http.get(`${this.apiUrl}/rooms`)
             .toPromise()
             .then(response => response.json() as Room[])
             .catch(this.handleError);
  }

  getRoomsInCity(city: number): Promise<Room[]> {
    return this.getRooms().then(rooms => {
      if (city > 0 && city <= 3) {
        return rooms.filter(room => room.city === city);
      } else {
        return rooms;
      }
    });
  }

  getCity(id: number): Promise<City> {
    return this.http.get(`${this.apiUrl}/cities/${id}`)
             .toPromise()
             .then(response => response.json() as City)
             .catch(this.handleError);
  }

  getBuilding(id: number): Promise<Building> {
    return this.http.get(`${this.apiUrl}/buildings/${id}`)
             .toPromise()
             .then(response => response.json() as Building)
             .catch(this.handleError);
  }

  getRoom(id: number): Promise<Room> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Room)
      .catch(this.handleError);
  }

  getFreeRooms(city: number, day: Date, from: Date, to: Date): Promise<FreeRoom[]> {
    // TODO check if params have the right datatypes
    // console.log(from.getDate() + "--" + to.getDate());
    return this.tmtblServ.getLecturesInCityFromTo(city, day, from, to)
      .then(
        lectures => this.calcFreeRooms(city, lectures, from, to)
      );
  }

  calcFreeRooms(city: number, lectures: Lecture[], from: Date, to: Date): Promise<FreeRoom[]> {
    // TODO to refine the choosing of the rooms
    return this.getRoomsInCity(city)
      .then(rooms => {
        const freeRooms = new Array<FreeRoom>();

        for (const r of rooms) {
          // let fr = new FreeRoom();

          let freeTimeSlots = new Array<[Date, Date]>();
          // console.log(from.toString() + "--" + to.toString());
          freeTimeSlots = this.AddTuple(freeTimeSlots, [from, to]);
          console.log("beginning: " +from.toString());
          let isRoomFree = true;

          for (const l of lectures) {
            // console.log("dsfsdf");
            if (r.name === l.Room) {
              if (l.Start <= from && l.End >= to) {
                 isRoomFree = false;
                 // console.log(r.name + ": " + "-----------------------------FULL  " + l.End+ "-" + to);
                 break;
              } else {
                freeTimeSlots = this.UpdateFreeTimeSlots(freeTimeSlots, l.Start, l.End, from, to);
                // if(freeTimeSlots!= undefined)  console.log(r.name + ": " + freeTimeSlots[0][0] + "----" + freeTimeSlots[0][1]);
              }
            }
          }
          if (isRoomFree && freeTimeSlots !== undefined) {
            for (const couple of freeTimeSlots){
              const fr = new FreeRoom();
              fr.Room = r;
              fr.From = couple[0];
              fr.To = couple[1];
              // console.log(fr);
              freeRooms.push(fr);
              console.log(fr.Room.name + ': ' + couple[0] + '----' + couple[1]);
            }
          }
        }
        // console.log("number of free rooms" + freeRooms.length);
        return freeRooms;
      })
      .catch(this.handleError);
    }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  private AddTuple(tuple: Array<[Date, Date]>, newTuple: [Date, Date]): Array<[Date, Date]> {
    if (newTuple[0].getHours() * 100 + newTuple[0].getMinutes() < newTuple[1].getHours() * 100 + newTuple[1].getMinutes()) {
      tuple.push(newTuple);
    }
    // console.log('new tuple ' + newTuple[0].getHours() + '||' + newTuple[1].getHours());
    return tuple;
  }

  private UpdateFreeTimeSlots(freeTimeSlots: Array<[Date, Date]>,
    lectStart: Date, lectEnd: Date, inputFrom: Date, inputTo: Date): Array<[Date, Date]> {
    let tmpTimeSlots = new Array<[Date, Date]>();
      // input bounds
      const boundFrom = inputFrom;
      const boundTo = inputTo;
      // lecture bounds
      const start = lectStart;
      const end = lectEnd;
    // the lecture is inside our chosen counds
    if (!(end <= boundFrom || start >= boundTo)) {
      // console.log('found a free time');
      for (const times of freeTimeSlots){
        // pre founded times

        const from = times[0];
        const to = times[1];
        // console.log('timeSlot checking ' + times[0].getHours() + '||' + times[1].getHours());
        // split the times
        if (start > from && end < to) {
          tmpTimeSlots = this.AddTuple(tmpTimeSlots, [inputFrom, lectStart]);
          tmpTimeSlots = this.AddTuple(tmpTimeSlots, [lectEnd, inputTo]);
          // console.log('free time --------SPLIT');
        } else if (start <= from && end >= from && end < to) {
          times[0] = lectEnd;
          tmpTimeSlots = this.AddTuple(tmpTimeSlots, times);
          // console.log('free time --------RIGHT');
        } else if (start > from && start < to && end > to) {
          times[1] = lectStart;
          tmpTimeSlots = this.AddTuple(tmpTimeSlots, times);
          // console.log('free time --------LEFT');
        }
      }
    }
    return tmpTimeSlots;
  }
}
