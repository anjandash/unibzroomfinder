import { TestBed, inject } from '@angular/core/testing';

import { InMemoryRoomService } from './static-rooms-web-api.service';

describe('InMemoryRoomService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InMemoryRoomService]
    });
  });

  // it('should be created', inject([InMemoryRoomService], (service: InMemoryRoomService) => {
  //   expect(service).toBeTruthy();
  // }));
});
