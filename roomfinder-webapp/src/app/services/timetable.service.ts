import { Injectable, Inject } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Lecture } from '../classes/lecture';
import { City } from '../classes/city';

@Injectable()
export class TimetableService {

  private apiUrl = 'http://' + window.location.hostname + ':3000';

  private cachedLectures: CachedLectures;

  constructor(private http: Http) { }

  getTest(): Promise<String[]> {
    const url = `${this.apiUrl}/test`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data)
      .catch(this.handleError);
  }

  getLecturesTest(): Promise<Lecture[]> {
    const url = `${this.apiUrl}/test`;
    return this.http.get(url)
      .toPromise()
      .then(response => {
        if (response.status >= 400) {
          return this.handleError(response.statusText);
        }
        return response.json() as Lecture[];
      })
      .catch(e => this.handleError(e));
  }

  getLecturesInCityForDay(city: number, day: Date): Promise<Lecture[]> {
    return this.lecturesHttpRequest(city, day)
    .then(lectures => this.fixLectureParams(lectures));
  }

  private lecturesHttpRequest(city: number, day: Date): Promise<Lecture[]> {
    let query = '?';

    if (this.cachedLectures != null && this.cachedLectures.equals(day, city)) {
      return Promise.resolve(this.cachedLectures.lectures);
    } else {
      // TODO check if from is smaller than today
      // TODO include a filter for the city
      if (City.isValidCityId(city)) {
        query += 'city=' + city;
      }
      const today = new Date();
      if (day == null || day.getTime() <= today.getTime()) {
        day = today;
      }
      query += '&day=' + day.getFullYear() + '-' + (day.getMonth() + 1) + '-' + day.getDate();
      const url = `${this.apiUrl}/lectures` + query;
      // console.log(url);

      return this.http.get(url)
        .toPromise()
        .then(response => response.json() as Lecture[])
        .catch(this.handleError);
    }
  }
  private fixLectureParams(lectures: Lecture[]): Lecture[] {
    for (const l of lectures){
      l.Start = new Date(l.Start);
      l.End = new Date(l.End);
      l.Date = new Date(l.Date);
      // console.log(l.Start+ "->" + l.End);
    }
    return lectures;
  }

  public getLecturesForDay(day: Date): Promise<Lecture[]> {
    return this.getLecturesInCityForDay(City.CITY_ID_INVALID, day);
  }

  // the filtering finds nothing TOFIX
  getLecturesInCityFromTo(city: number, day: Date, from: Date, to: Date): Promise<Lecture[]> {
    return this.getLecturesInCityForDay(city, day)
      .then(lectures => {
        // console.log('lectures: ' + lectures.length);
        const allLectures = lectures;
        const boundFrom = from; // .valueOf();
        const boundTo = to; // .valueOf();
        const filteredLectures = allLectures.filter(lect => {
          const start = new Date(lect.Start); // lect.Start.valueOf();
          const end = new Date(lect.End); // lect.End.valueOf();
          // console.log(lect.Start + '--' + lect.End);
          // TOFIX the filter doesn't work
          return lect; /*(lect.End >= boundFrom && lect.End <= boundTo) ||
                 (lect.Start <= boundTo && lect.Start >= boundFrom) ||
                 (lect.Start <= boundFrom && lect.End >= boundTo);*/
        });
        // console.log('filtered lectures: ' + filteredLectures.length);
        return filteredLectures;
      })
      .catch(this.handleError);
  }

  getLecturesFromTo(day: Date, from: Date, to: Date): Promise<Lecture[]> {
    return this.getLecturesInCityFromTo(0, day, from, to);
  }

  updateLecturesCache(city: number, day: Date) {
    if (day != null) {
      this.getLecturesInCityForDay(city, day)
        .then(lectures => this.cachedLectures = CachedLectures.createWithDateAndLectureAndCity(day, lectures, city));
    }
  }

  private handleError(error: any): Promise<any> {
    // console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

export class CachedLectures {
  day: Date;
  city: number;
  lectures: Lecture[];

  constructor() {
  }

  static createWithDateAndLecture(day: Date, lectures: Lecture[]): CachedLectures {
    return CachedLectures.createWithDateAndLectureAndCity(day, lectures, City.CITY_ID_INVALID);
  }

  static createWithDateAndLectureAndCity(day: Date, lectures: Lecture[], city: number): CachedLectures {
    const cl = new CachedLectures();
    cl.day = day;
    cl.city = city;
    cl.lectures = lectures;
    return cl;
  }

  equals(day: Date, city: number): boolean {
    return this.day.getTime() === day.getTime() && this.city === city;
  }
}
