import { ReflectiveInjector } from '@angular/core';
import {async, fakeAsync, tick} from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, RequestOptions } from '@angular/http';
import {Response, ResponseOptions} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { TimetableService } from './timetable.service';
import { Lecture } from '../classes/lecture';


describe('MockBackend TimetableService', () => {
  const heroService: TimetableService = null;
  const backend: ConnectionBackend = null;

  beforeEach(() => {
    this.injector = ReflectiveInjector.resolveAndCreate([
      {provide: ConnectionBackend, useClass: MockBackend},
      {provide: RequestOptions, useClass: BaseRequestOptions},
      Http,
      TimetableService,
    ]);
    this.timetableService = this.injector.get(TimetableService);
    this.backend = this.injector.get(ConnectionBackend) as MockBackend;
    this.backend.connections.subscribe((connection: any) => this.lastConnection = connection);
  });

  it('getLecturesTest() should query current service url', () => {
    this.timetableService.getLecturesTest();
    expect(this.lastConnection).toBeDefined('no http service connection at all?');
    expect(this.lastConnection.request.url).toMatch(/test$/, 'url invalid');
  });

  it('getLecturesTest() should return some testdata - fakeAsync', fakeAsync(() => {
    let result: Lecture[];

    this.timetableService.getLecturesTest().then((lectures: Lecture[]) => result = lectures);

    this.lastConnection.mockRespond(new Response(new ResponseOptions({
      body: JSON.stringify(mockResponseLectures),
    })));

    tick();
    expect(result.length).toEqual(2, 'should contain given amount of data objects');
    expect(result[0].ActivityDescription)
      .toEqual('Analisi Matematica 1',
               'Analisi Matematica 1 should be the ActivityDescription of the first object');
    expect(result[1].ActivityDescription)
      .toEqual('Advanced topics on machine design',
               'Advanced topics on machine design should be the ActivityDescription of the first object');
  }));

  it('getLecturesTest() should return some testdata', () => {
    this.timetableService.getLecturesTest().then((lectures: Lecture[]) => {
      expect(lectures.length).toEqual(2, 'should contain given amount of data objects');
      expect(lectures[0].ActivityDescription)
      .toEqual('Analisi Matematica 1',
      'Analisi Matematica 1 should be the ActivityDescription of the first object');
      expect(lectures[1].ActivityDescription)
      .toEqual('Advanced topics on machine design',
      'Advanced topics on machine design should be the ActivityDescription of the first object');
    });

    this.lastConnection.mockRespond(new Response(new ResponseOptions({
      body: JSON.stringify(mockResponseLectures),
    })));

  });

  it('getLecturesTest() while server is down', fakeAsync(() => {
    let result: Lecture[];
    let catchedError: any;
    this.timetableService.getLecturesTest()
        .then((lectures: Lecture[]) => result = lectures)
        .catch((error: any) => catchedError = error);
    this.lastConnection.mockRespond(new Response(new ResponseOptions({
      status: 404,
      statusText: 'URL not Found',
    })));
    tick();
    expect(result).toBeUndefined();
    expect(catchedError).toBeDefined();
  }));
  
  it('getLecturesInCityForDay(city: number, day: Date) for the 6th should return something', fakeAsync(() => {
    let result: Lecture[];
    let startDate = new Date('2017-12-06T00:00:00' );
    let endDate   = new Date('2017-12-06T23:59:99 ' );
    this.timetableService.getLecturesInCityForDay(1, startDate).then((fr: Lecture[]) => result = fr);
    this.lastConnection.mockRespond(new Response(new ResponseOptions({
      body: JSON.stringify(mockResponseLectures),
    })));
    tick();
    expect(result.length).toBeGreaterThan(1);
  }));
});

const mockResponseLectures = [
  {
  Start: '2017-12-06T08:30:00',
  End: '2017-12-06T10:30:00',
  Date: '2017-12-06T00:00:00',
  HideEndTime: false,
  StaffAcademicTitle: 'Dr.',
  StaffLastname: 'Levaggi',
  StaffFirstname: 'Laura',
  ProfHrisId: 27466,
  ActivityDescription: 'Analisi Matematica 1',
  ActivityType: 'LECT',
  BuildingName: 'Ser-C',
  City: 'Bolzano',
  Room: 'BZ C4.01',
  RoomDescription: 'C4.01 Lecture room, Ser-C',
  RIS_departmentId: 9475,
  DepartmentId: 370,
  ProfImageUrl: 'https://webservices.scientificnet.org/rest/cm/core/api/Image/Get/?domain=unibz&username=llevaggi'
  },
  {
  Start: '2017-12-06T08:30:00',
  End: '2017-12-06T10:30:00',
  Date: '2017-12-06T00:00:00',
  HideEndTime: false,
  StaffAcademicTitle: 'Dr. Ing.',
  StaffLastname: 'Concli',
  StaffFirstname: 'Franco',
  ProfHrisId: 34279,
  ActivityDescription: 'Advanced topics on machine design',
  ActivityType: 'EXERCISE',
  BuildingName: 'Ser-C',
  City: 'Bolzano',
  Room: 'BZ C2.02',
  RoomDescription: 'C2.02 Seminar room, Ser-C',
  RIS_departmentId: 9475,
  DepartmentId: 370,
  ProfImageUrl: 'https://webservices.scientificnet.org/rest/cm/core/api/Image/Get/?domain=unibz&username=FConcli'
  }
];
