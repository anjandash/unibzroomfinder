import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { Http, Headers } from '@angular/http';

import { RoomService } from '../services/room.service';
import { TimetableService } from '../services/timetable.service';
import { FreeRoom } from '../classes/free-room';
import { Lecture } from '../classes/lecture';
import { City } from '../classes/city';
import { Room } from '../classes/room';
import { log } from 'util';


@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  public cityOptions = City.CITY_OPTIONS;
  public buildingOptions;
  public freeRooms: Promise<FreeRoom[]>;
  public newfreeRooms: Promise<FreeRoom[]>;
  public lectures: Promise<Lecture[]>;
  public rooms: Room[];

  public mainform= false;
  public subform= true;
  public Filter;

  // form components
  public city;
  public building;
  public dateFrom;
  public fromHours;
  public fromMinutes;
  public toHours;
  public toMinutes;
  public selcity;
  public from;
  public to;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private http: Http,
    private roomService: RoomService,
    private tmtblService: TimetableService,
  ) {
    this.city = this.cityOptions[0].id;
    this.buildingOptions = [];
    this.updateBuildingChoice();
    this.dateFrom = new Date();
    // console.log(this.dateFrom);
    this.from = new Date(this.dateFrom);
    this.from.setHours(8);
    this.from.setMinutes(0);
    this.fromHours = this.from.getHours();
    this.fromMinutes = this.from.getMinutes();
    this.to = new Date(this.dateFrom);
    this.to.setHours(10);
    this.to.setMinutes(0);
      this.toHours = this.to.getHours();
    this.toMinutes = this.to.getMinutes();
    //console.log("FROM: " + this.from.toString() + this.from.valueOf() + "--" + this.to.toString() + this.to.valueOf() );

    this.Filter = 'All';
    this.selcity = -1;

    this.cacheLectures();
  }

  ngOnInit() {
    this.getRoomsInCity(this.selcity);
  }

  onSubmit(f: NgForm) {
    const city = this.city;
    // console.log(f.value);

    const day = new Date(this.dateFrom);
    // console.log(this.from.toString());
    // console.log(this.to.toString());
    // console.log(f.value.fromHours);
    // console.log(f.value.fromMinutes);

    this.from.setHours(f.value.fromHours);
    this.from.setMinutes(f.value.fromMinutes);
    this.from.setSeconds(0);
    this.to.setHours(f.value.toHour);
    this.to.setMinutes(f.value.toMin);
    this.to.setSeconds(0);
    console.log("from" + this.from);
    console.log("to" + this.to);

    this.onChangeFilter('All');

    this.lectures = this.tmtblService.getLecturesInCityFromTo(city, day, this.from, this.to).then(lectures => {
      // console.log(lectures.toString());
      return lectures;
    });
    this.freeRooms = this.roomService.getFreeRooms(city, day, this.from, this.to)
      .then(freeRooms => {
        if (this.building > 0) {
          freeRooms = freeRooms.filter(fr => fr.Room.building === this.building );
        }
        return freeRooms.sort((fr1, fr2) => Room.compare(fr1.Room, fr2.Room));
    });
  }

  onSubSubmit(r: NgForm) {
    const city = this.selcity;
    const day = new Date(this.dateFrom);

    this.from.setHours(8);
    this.from.setMinutes(30);
    this.to.setHours(18);
    this.to.setMinutes(0);

    this.lectures = this.tmtblService.getLecturesInCityFromTo(city, day, this.from, this.to).then(lectures => {
      // console.log(lectures.toString());
      return lectures;
    });
    this.newfreeRooms = this.roomService.getFreeRooms(city, day, this.from, this.to)
      .then(newfreeRooms => {
        return newfreeRooms.sort((fr1, fr2) => Room.compare(fr1.Room, fr2.Room));
    });
  }

  setCityOptions() {
    this.getRoomsInCity(this.selcity);

    // reseting search result
    this.freeRooms = null;
    this.newfreeRooms = null;
  }

  onSelectTime() {
    this.subform = true;
    this.mainform = false;

    // reseting search result
    this.freeRooms = null;
    this.newfreeRooms = null;
  }

  onSelectRoom() {
    this.mainform = true;
    this.subform = false;

    // reseting search result
    this.freeRooms = null;  
    this.newfreeRooms = null;  
  }

  onChangeFilter(value: string) {
    // console.log(value);
  }

  getRoomsInCity(selcity): void {
    this.roomService.getRoomsInCity(selcity).then(rooms => this.rooms = rooms.sort((r1, r2) => Room.compare(r1, r2)));
  }

  public onSessionChange(value: string) {

  // console.log(value);
  if (value === 'morning') {

    this.from.setHours(8);
    this.from.setMinutes(30);
    this.to.setHours(13);
    this.to.setMinutes(0);

    this.updateFromAndToField();
  }

  if (value === 'afternoon') {
    this.from.setHours(14);
    this.from.setMinutes(0);
    this.to.setHours(18);
    this.to.setMinutes(0);
    this.updateFromAndToField();
  }

  if (value === 'evening') {
    this.from.setHours(18);
    this.from.setMinutes(0);
    this.to.setHours(22);
    this.to.setMinutes(0);
    this.updateFromAndToField();
  }

  if (value === 'allday') {
    this.from.setHours(8);
    this.from.setMinutes(30);
    this.to.setHours(18);
    this.to.setMinutes(0);
    this.updateFromAndToField();
  }
  // console.log(this.from.toString() + this.from.valueOf() + '--' + this.to.toString() + this.to.valueOf() );

  }

  locationChanged() {
    this.cacheLectures();
    this.updateBuildingChoice();
  }

  cacheLectures() {
    const date = new Date(this.dateFrom);
    this.tmtblService.updateLecturesCache(this.city, date);
  }

  public updateFromAndToField() {
    this.fromHours = this.from.getHours();
    this.fromMinutes = this.from.getMinutes();

    this.toHours = this.to.getHours();
    this.toMinutes = this.to.getMinutes();
  }

  updateBuildingChoice() {
    this.roomService.getBuildingsInCity(this.city)
      .then(buildings => this.buildingOptions = buildings);
  }
}
