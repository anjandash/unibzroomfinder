var express = require('express');
var request = require('request');
var app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/test', function(req, res) {
  var url = 'https://webservices.scientificnet.org/rest/cm/core/api/Timetable/GetEvents/?q={"Departments":[],"Cities":[],"ActivityTypes":[],"OnlyCurrent":false,"Day":"2017-12-06T00:00:00Z","MinutesToBegin":30}';
  console.log(url);  
  request(url, function(error, response, body) {
      res.send(body)
  });
});

/*
 * Parameters
 * day: in the following format: "2017-10-05"
 * city: a number (1=BZ, 2=BX, 3=BK)
**/
app.get('/lectures', function(req, res) {
  var day = req.query.day;
  var city = req.query.city;

  //TODO some errorhandling when no date provided etc...
  
  var url = 'https://webservices.scientificnet.org/rest/cm/core/api/Timetable/GetEvents/?' 
  + 'q={"Departments":[],' 
  + '"Cities":[' + (city ? city : '') + '],' 
  + '"ActivityTypes":[],' 
  + '"OnlyCurrent":false,' 
  + '"Day":"' + day + 'T00:00:00Z",' 
  + '"MinutesToBegin":30}';
  console.log(url);
  
  request(url, function(error, response, body) {
      res.send(body)
  });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});