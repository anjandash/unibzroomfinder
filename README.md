# README #

This is an application to search for available rooms at a given point @unibz.

### What is this repository for? ###

Version 1.0.0
https://bitbucket.org/anjandash/unibzroomfinder

### How do I get set up? ###

* Summary of set up
	1. Have a look at dependencies.
	2. Run the roomfinder-api:
		Navigate the terminal to the folder roomfinder-api and run:
			`node api.js`
		This runs the custom api, that serves as a proxy to make sure we can access the data that comes from UniBZ servers.
		For Development the api runs on http://localhost:3000
	3. Run the web application:
		Navigate the terminal to the folder roomfinder-webapp and run:
			`npm start`
			or
			`ng serve`
		This fires up the Angular application that should now be accessible through the browser on http://localhost:4200
* Configuration
* Dependencies
	1. Be sure to have Node.js and Angular installed on your machine.
		Node.js: https://nodejs.org/en/download/
		Angular: https://angular.io/guide/quickstart
	2. Navigate into the folder roomfinder-api in the terminal and run 
		`npm install`
	3. Navigate into the folder roomfinder-webapp in the terminal and run 
		`npm install`
Now your dependencies should be installed.
It might be necessary to reinstall dependencies, after certain commits. That will be mentioned in the commitmessage.
	
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact